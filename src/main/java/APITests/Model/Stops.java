package APITests.Model;

//import javax.json.Json;
//import javax.json.JsonObject;
//import javax.json.JsonObjectBuilder;

public class Stops {
	private double lat;
	private double lng;
	//public JsonObjectBuilder builder = Json.createObjectBuilder();
	
	public double getLat() {
		return lat;
	}	

	public double getLng() {
		return lng;
	}
	
	public void setLatLng(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}
}
