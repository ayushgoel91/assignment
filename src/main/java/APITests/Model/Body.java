package APITests.Model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class Body {	
	String orderAt;
	List<Stops> stops;
	
	public String getOrderAt() {
		return orderAt;
	}
	public void setDateAfterDays(int futureDays) {
		if (futureDays <= 0)
			futureDays = 1;
		Calendar cal = Calendar.getInstance();
		TimeZone timezone = TimeZone.getDefault();
		cal.add(Calendar.DATE, futureDays);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		dateFormat.setTimeZone(timezone);
		this.orderAt = dateFormat.format(cal.getTime());
	}
	public List<Stops> getStops() {
		return stops;
	}
	public void setStops(List<Stops> stops) {
		this.stops = stops;
	}
}
