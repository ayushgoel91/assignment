package APITests.Assignment;

import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

import java.util.List;

import APITests.Model.Body;

/**
 * Hello world!
 *
 */
public class HelperMethods 
{
	public Response response;
	
    public Response getRequest(String url) {
    	return given().get(url);
    }
    
    public Response putRequest(String url) {
    	return given().when().put(url);
    }
    
    public Response postRequest(String url, Body body) {    	
    	return given().when().contentType("application/json").body(body).log().all().post(url);
    }	

	public Double calculateFare(List<Integer> list, String orderAt) {
		int start = orderAt.indexOf('T');
		int end = orderAt.indexOf(':');
		String substring = orderAt.substring(start+1, end);
		int hour = Integer.parseInt(substring);
		int totalDistance = 0;
		
		for (Integer i : list) {
			totalDistance = totalDistance + i;
		}
		
		if (hour < 21 || hour > 5 ) {
			return (totalDistance/1000 > 2)?(20 + (totalDistance - 2000)*0.025):(totalDistance*0.025);
		}else {
			return (totalDistance/1000 > 2)?(30 + (totalDistance - 2000)*0.04):(totalDistance*0.04);
		}
	}
}
