package APITests.Assignment;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.testng.asserts.SoftAssert;

import APITests.Model.Body;
import APITests.Model.Stops;
import io.restassured.RestAssured;
import io.restassured.response.Response;

/** 
 * @author ayush
 * Test cases to test APIs
 */

public class AppTest{
	HelperMethods helper = new HelperMethods();
	int orderId;
	int nonExistingOrderId = 0;
	String currentStatus;
	String[] statuses = {"ASSIGNING", "ONGOING", "CANCELLED", "COMPLETED"};
	
	@BeforeClass
	public static void initialize() {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 51544;
		RestAssured.basePath = "/v1";
	}
	
	// Test to get non existing order (Test_001)
	@Test
	public void getNonExistingOrderTest() {
		
		// Sending request
		Response res = helper.getRequest("/orders/" + nonExistingOrderId);
		
		// Printing the response
		res.body().prettyPrint();
		
		// Assertions
		Assert.assertEquals(res.statusCode(), 404);
		Assert.assertEquals(res.jsonPath().getString("message"), "ORDER_NOT_FOUND");
	}
	
	// Test to get existing order (Test_002)
	@Test (dependsOnMethods = "placeOrderTest", priority = 1)
	public void getExistingOrderTest() {
		
		// Sending request
		Response res = helper.getRequest("/orders/" + orderId);
		
		// Printing the response
		res.body().prettyPrint();
		
		// Extracting status
		currentStatus = res.jsonPath().getString("status");
				
		// Assertions
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(res.statusCode(), 200);
		softAssert.assertEquals(res.jsonPath().getInt("id"), orderId);		
		softAssert.assertTrue(ArrayUtils.contains(statuses, currentStatus));
		softAssert.assertNotEquals(res.jsonPath().getString("orderDateTime"), "");
		softAssert.assertNotEquals(res.jsonPath().getString("createdTime"), "");
		softAssert.assertAll();
	}	
	
	// Test to place order (Test_003)
	@Test
	public void placeOrderTest() {
		
		// Test setup
		Body body = new Body();
		Stops st1 = new Stops();
		Stops st2 = new Stops();
		Stops st3 = new Stops();
		st1.setLatLng(22.344674, 114.124651);
		st2.setLatLng(22.375384, 114.182446);
		st3.setLatLng(22.385669, 114.186962);		
		
		ArrayList<Stops> stops = new ArrayList<Stops>();
		stops.add(st1);
		stops.add(st2);
		stops.add(st3);
		
		body.setDateAfterDays(2);
		body.setStops(stops);	
		
		// Sending request
		Response res = helper.postRequest("/orders", body);
		res.then().log().body();
		
		// Extracting response
		List<Integer> distances = res.then().extract().path("drivingDistancesInMeters");
		Double fare = helper.calculateFare(distances, body.getOrderAt());
		orderId = res.jsonPath().getInt("id");
		
		// Assertions
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(res.getStatusCode(), 201);
		softAssert.assertTrue(orderId > 0);
		softAssert.assertEquals(res.jsonPath().getString("fare.currency"), "HKD");
		softAssert.assertEquals(res.jsonPath().getDouble("fare.amount"), fare);
		softAssert.assertAll();		
	}	
	
	// Test to change status ASSIGNING -> ONGOING (Test_004)
	@Test (dependsOnMethods = "placeOrderTest", priority = 3)
	public void changeAssignToOngoingTest() {
		
		// Sending request
		Response res = helper.putRequest("/orders/" + orderId + "/take");
		Response getRes = helper.getRequest("/orders/" + orderId);
		
		// Assertions
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(res.statusCode(), 200);
		softAssert.assertEquals(res.jsonPath().getString("status"), getRes.jsonPath().getString("status"));
		softAssert.assertAll();		
	}
	
	// Test to change status of non existing order (Test_005)
	@Test
	public void changeStatusNonExistingOrderTest() {
		
		// Sending request
		Response res = helper.putRequest("/orders/" + nonExistingOrderId + "/take");		
		
		// Assertions
		Assert.assertEquals(res.statusCode(), 404);
		Assert.assertEquals(res.jsonPath().getString("message"), "ORDER_NOT_FOUND");		
	}
	
	// Test to change status ONGOING -> CANCEL (Test_006)
	@Test (dependsOnMethods = "placeOrderTest", priority = 4)
	public void changeOngoingToCancelTest() {
		
		// Sending request
		Response res = helper.putRequest("/orders/" + orderId + "/cancel");
		Response getRes = helper.getRequest("/orders/" + orderId);
		
		// Assertions
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(res.statusCode(), 200);
		softAssert.assertEquals(res.jsonPath().getString("status"), getRes.jsonPath().getString("status"));
		softAssert.assertAll();		
	}		
	
	// Test to change status ASSIGING -> CANCEL (Test_006)
	@Test (dependsOnMethods = "placeOrderTest", priority = 5)
	public void changeAssigningToCancelTest() {
		
		// Posting an order
		this.placeOrderTest();
		
		// Sending request
		Response res = helper.putRequest("/orders/" + orderId + "/cancel");
		Response getRes = helper.getRequest("/orders/" + orderId);
		
		// Assertions
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(res.statusCode(), 200);
		softAssert.assertEquals(res.jsonPath().getString("status"), getRes.jsonPath().getString("status"));
		softAssert.assertAll();		
	}
	
	// Test to change status ONGOING -> COMPLETE (Test_008)
	@Test (dependsOnMethods = "placeOrderTest", priority = 6)
	public void changeOngoingToCompleteTest() {
		
		// Posting an order
		this.placeOrderTest();
		
		// Sending request to change status to ONGOING
		Response res = helper.putRequest("/orders/" + orderId + "/take");
		Assert.assertEquals(res.statusCode(), 200);
		
		// Sending request to change status from ONGOING -> COMPLETE
		res = helper.putRequest("/orders/" + orderId + "/complete");
		Response getRes = helper.getRequest("/orders/" + orderId);
		
		// Assertions
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(res.statusCode(), 200);
		softAssert.assertEquals(res.jsonPath().getString("status"), getRes.jsonPath().getString("status"));
		softAssert.assertAll();		
	}
	
	// Test for Logic flow violation ASSIGINING -> COMPLETE (Test_010)
	@Test (dependsOnMethods = "placeOrderTest", priority = 2)
	public void changeAssignToCompleteTest() {
		
		// Sending request
		Response res = helper.putRequest("/orders/" + orderId + "/complete");
		
		// Assertions
		Assert.assertEquals(res.statusCode(), 422);
		Assert.assertEquals(res.jsonPath().getString("message"), "Order status is not ONGOING");
	}	
		
	
}
